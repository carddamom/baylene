# Baylene

This is a tool to run continous integration style builds on demand using docker images.

## Features

- Easy to use;
- Replicates most continous integration style builds;

## Roadmap

- Add a build file manager, for frequent builds?
- Support more build styles;

## Coding

Most important layers of baylene:

- Argument processing;
- Configuration management;
- Docker management;
- Build file parser;
- Log file management;

## Argument processing

Supported commands:

- validate => Validates the given build file;
- version => Shows versioning information;
- run => Runs the build;
- convert => Converts between supported build files;

### Validate Command

Supported options:

- strict, s => If a strict mode is to be used;
- format, f => The format of the input build file;
- mode, m => The mode of output (machine or human readable);

### Version Command

No options needed

### Run Command

Supported options:

- format, f => The format of the input build file;
- mode, m => The mode of output (machine or human readable);

### Convert Command

Supported options:

- iformat => The format of the input build file;
- oformat => The format of the output build file;
- input, i => The input build file;
- output, o => The output build file;
- mode, m => The mode of output (machine or human readable);

### Global Options

- verbose, v => If more verbose output is required;
- config, c => The configuration file to use;
- dryrun, n => It makes the command execute everything but without doing its action just printing what it would do;

### Coding Details

Implemented using kingpin and as a collection of classes implementing the following interface (Command):

- func PreRun( ctx *kingpin.ParseContext) error
- func Run( ctx *kingpin.ParseContext) error
- func Validate( ctx *kingpin.CmdClause) error
- func Configure(cmd *kingpin.CmdClause) error


