// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	command "gitlab.com/carddamom/baylene/lib/command"
)

var (
	logger *log.Logger
)

func init() {
	logger = log.New()
	logger.SetFormatter(&log.TextFormatter{
		FullTimestamp: true,
	})
	logger.SetLevel(log.InfoLevel)
}

func main() {
	commands := make(map[string]command.Command)
	commands["lint"] = command.NewLintCommand(logger)
	commands["add"] = command.NewAddCommand(logger)
	commands["remove"] = command.NewRemoveCommand(logger)
	commands["list"] = command.NewListCommand(logger)
	commands["run"] = command.NewRunCommand(logger)

	app := cli.NewApp()
	app.Name = "Baylene"
	app.Authors = []cli.Author{{Name: "carddamom", Email: "carddamom@outlook.pt"}}
	app.Description = "Do continous integration style builds, on demand using docker images"
	app.Usage = "The local continous integration tool"
	app.Version = "1.0.0"
	app.Commands = make([]cli.Command, 5)
	app.Commands[0] = cli.Command{
		Name:    "lint",
		Aliases: []string{"l"},
		Usage:   "Verifies the baylene-descriptor file",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:  "project",
				Usage: "The path to the project to verify the descriptor",
			},
		},
		Before: commands["lint"].Before,
		Action: commands["lint"].Run,
		After:  commands["lint"].After,
	}
	app.Commands[1] = cli.Command{
		Name:    "add",
		Aliases: []string{"a"},
		Usage:   "Adds the given project to the list of projects known by baylene",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:  "project",
				Usage: "The path to the project to add",
			},
			cli.StringFlag{
				Name:  "name",
				Usage: "The registration name of the project",
			},
			cli.StringFlag{
				Name:  "configuration",
				Usage: "The configuration file to use for the project",
				Value: "~/.baylene.conf",
			},
		},
		Before: commands["add"].Before,
		Action: commands["add"].Run,
		After:  commands["add"].After,
	}
	app.Commands[2] = cli.Command{
		Name:    "remove",
		Aliases: []string{"rm"},
		Usage:   "Removes the given project from the list of projects known by baylene",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:  "name",
				Usage: "The registration name of the project",
			},
			cli.StringFlag{
				Name:  "configuration",
				Usage: "The configuration file to use for the project",
				Value: "~/.baylene.conf",
			},
		},
		Before: commands["remove"].Before,
		Action: commands["remove"].Run,
		After:  commands["remove"].After,
	}
	app.Commands[3] = cli.Command{
		Name:  "list",
		Usage: "list the projects known by baylene",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:  "configuration",
				Usage: "The configuration file to use for the project",
				Value: "~/.baylene.conf",
			},
		},
		Before: commands["list"].Before,
		Action: commands["list"].Run,
		After:  commands["list"].After,
	}
	app.Commands[4] = cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Runs the build for the given project",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:  "name",
				Usage: "The registration name of the project",
			},
			cli.StringFlag{
				Name:  "configuration",
				Usage: "The configuration file to use for the project",
				Value: "~/.baylene.conf",
			},
		},
		Before: commands["run"].Before,
		Action: commands["run"].Run,
		After:  commands["run"].After,
	}
	err := app.Run(os.Args)
	if err != nil {
		logger.Errorf("Error executing baylene: %s\n", err.Error())
	}
}
