// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package utilities

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSliceStructures(t *testing.T) {

	Convey("Given a BooleanSlice", t, func() {
		var test *BoolSlice

		Convey("It should be able to create one from a bool slice", func() {
			tmp := []bool{true, false, true, true}
			test = NewBoolSlice(tmp)
			So(test.element[1], ShouldBeFalse)
			So(test.element[0], ShouldBeTrue)
			So(test.element[2], ShouldBeTrue)
			So(test.element[3], ShouldBeTrue)
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewBoolSlice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []bool{true, true, true, true}
			test = NewBoolSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(false)
			So(test.element[4], ShouldBeFalse)
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf(false)
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []bool{true, true, true, true}
			test = NewBoolSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(false).Remove(false)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(false)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(true)
			So(len(test.element), ShouldEqual, 0)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []bool{true, true, true, true}
			test = NewBoolSlice(tmp)
			So(test.Contains(true), ShouldBeTrue)
			So(test.Contains(false), ShouldBeFalse)
		})

		Convey("It should be able to cast it back to a bool slice", func() {
			tmp := []bool{true, false, true, true}
			test = NewBoolSlice(tmp)
			tmp2 := test.Cast()
			So(tmp2[1], ShouldBeFalse)
			So(tmp2[0], ShouldBeTrue)
			So(tmp2[2], ShouldBeTrue)
			So(tmp2[3], ShouldBeTrue)
		})

	})

	Convey("Given a StringSlice", t, func() {
		var test *StringSlice

		Convey("It should be able to create one from a string slice", func() {
			tmp := []string{"true", "false", "egg", "spam"}
			test = NewStringSlice(tmp)
			So(test.element[0], ShouldEqual, "true")
			So(test.element[1], ShouldEqual, "false")
			So(test.element[2], ShouldEqual, "egg")
			So(test.element[3], ShouldEqual, "spam")
		})

		Convey("It should be able to cast it back to a string slice", func() {
			tmp := []string{"true", "false", "egg", "spam"}
			test = NewStringSlice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, "true")
			So(tmp2[1], ShouldEqual, "false")
			So(tmp2[2], ShouldEqual, "egg")
			So(tmp2[3], ShouldEqual, "spam")
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewStringSlice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []string{"true", "false", "egg", "spam"}
			test = NewStringSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf("bacon")
			So(test.element[4], ShouldEqual, "bacon")
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf("spam")
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []string{"true", "false", "egg", "spam"}
			test = NewStringSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf("bacon").Remove("bacon")
			So(len(test.element), ShouldEqual, 4)
			test.Remove("bacon")
			So(len(test.element), ShouldEqual, 4)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []string{"true", "false", "egg", "spam"}
			test = NewStringSlice(tmp)
			So(test.Contains("egg"), ShouldBeTrue)
			So(test.Contains("bacon"), ShouldBeFalse)
		})

	})

	Convey("Given a IntSlice", t, func() {
		var test *IntSlice

		Convey("It should be able to create one from a int slice", func() {
			tmp := []int{0, 1, 1, 2}
			test = NewIntSlice(tmp)
			So(test.element[0], ShouldEqual, 0)
			So(test.element[1], ShouldEqual, 1)
			So(test.element[2], ShouldEqual, 1)
			So(test.element[3], ShouldEqual, 2)
		})

		Convey("It should be able to cast it back to a int slice", func() {
			tmp := []int{0, 1, 1, 2}
			test = NewIntSlice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, 0)
			So(tmp2[1], ShouldEqual, 1)
			So(tmp2[2], ShouldEqual, 1)
			So(tmp2[3], ShouldEqual, 2)
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewIntSlice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []int{0, 1, 1, 2}
			test = NewIntSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3)
			So(test.element[4], ShouldEqual, 3)
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf(1)
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []int{0, 1, 1, 2}
			test = NewIntSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3).Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(1)
			So(len(test.element), ShouldEqual, 2)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []int{0, 1, 1, 2}
			test = NewIntSlice(tmp)
			So(test.Contains(1), ShouldBeTrue)
			So(test.Contains(3), ShouldBeFalse)
		})

	})

	Convey("Given a UintSlice", t, func() {
		var test *UintSlice

		Convey("It should be able to create one from a uint slice", func() {
			tmp := []uint{0, 1, 1, 2}
			test = NewUintSlice(tmp)
			So(test.element[0], ShouldEqual, 0)
			So(test.element[1], ShouldEqual, 1)
			So(test.element[2], ShouldEqual, 1)
			So(test.element[3], ShouldEqual, 2)
		})

		Convey("It should be able to cast it back to a uint slice", func() {
			tmp := []uint{0, 1, 1, 2}
			test = NewUintSlice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, 0)
			So(tmp2[1], ShouldEqual, 1)
			So(tmp2[2], ShouldEqual, 1)
			So(tmp2[3], ShouldEqual, 2)
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewUintSlice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []uint{0, 1, 1, 2}
			test = NewUintSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3)
			So(test.element[4], ShouldEqual, 3)
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf(1)
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []uint{0, 1, 1, 2}
			test = NewUintSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3).Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(1)
			So(len(test.element), ShouldEqual, 2)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []uint{0, 1, 1, 2}
			test = NewUintSlice(tmp)
			So(test.Contains(1), ShouldBeTrue)
			So(test.Contains(3), ShouldBeFalse)
		})

	})

	Convey("Given a ByteSlice", t, func() {
		var test *ByteSlice

		Convey("It should be able to create one from a byte slice", func() {
			tmp := []byte{byte('t'), byte('f'), byte('e'), byte('s')}
			test = NewByteSlice(tmp)
			So(test.element[0], ShouldEqual, byte('t'))
			So(test.element[1], ShouldEqual, byte('f'))
			So(test.element[2], ShouldEqual, byte('e'))
			So(test.element[3], ShouldEqual, byte('s'))
		})

		Convey("It should be able to cast it back to a byte slice", func() {
			tmp := []byte{byte('t'), byte('f'), byte('e'), byte('s')}
			test = NewByteSlice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, byte('t'))
			So(tmp2[1], ShouldEqual, byte('f'))
			So(tmp2[2], ShouldEqual, byte('e'))
			So(tmp2[3], ShouldEqual, byte('s'))
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewByteSlice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []byte{byte('t'), byte('f'), byte('e'), byte('s')}
			test = NewByteSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(byte('b'))
			So(test.element[4], ShouldEqual, byte('b'))
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf(byte('s'))
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []byte{byte('t'), byte('f'), byte('e'), byte('s')}
			test = NewByteSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(byte('b')).Remove(byte('b'))
			So(len(test.element), ShouldEqual, 4)
			test.Remove(byte('b'))
			So(len(test.element), ShouldEqual, 4)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []byte{byte('t'), byte('f'), byte('e'), byte('s')}
			test = NewByteSlice(tmp)
			So(test.Contains(byte('e')), ShouldBeTrue)
			So(test.Contains(byte('b')), ShouldBeFalse)
		})

	})

	Convey("Given a RuneSlice", t, func() {
		var test *RuneSlice

		Convey("It should be able to create one from a rune slice", func() {
			tmp := []rune{'t', 'f', 'e', 's'}
			test = NewRuneSlice(tmp)
			So(test.element[0], ShouldEqual, 't')
			So(test.element[1], ShouldEqual, 'f')
			So(test.element[2], ShouldEqual, 'e')
			So(test.element[3], ShouldEqual, 's')
		})

		Convey("It should be able to cast it back to a rune slice", func() {
			tmp := []rune{'t', 'f', 'e', 's'}
			test = NewRuneSlice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, 't')
			So(tmp2[1], ShouldEqual, 'f')
			So(tmp2[2], ShouldEqual, 'e')
			So(tmp2[3], ShouldEqual, 's')
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewRuneSlice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []rune{'t', 'f', 'e', 's'}
			test = NewRuneSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf('b')
			So(test.element[4], ShouldEqual, 'b')
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf('s')
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []rune{'t', 'f', 'e', 's'}
			test = NewRuneSlice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf('b').Remove('b')
			So(len(test.element), ShouldEqual, 4)
			test.Remove('b')
			So(len(test.element), ShouldEqual, 4)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []rune{'t', 'f', 'e', 's'}
			test = NewRuneSlice(tmp)
			So(test.Contains('e'), ShouldBeTrue)
			So(test.Contains('b'), ShouldBeFalse)
		})

	})

	Convey("Given a Float32Slice", t, func() {
		var test *Float32Slice

		Convey("It should be able to create one from a float32 slice", func() {
			tmp := []float32{0, 1, 1, 2}
			test = NewFloat32Slice(tmp)
			So(test.element[0], ShouldEqual, 0)
			So(test.element[1], ShouldEqual, 1)
			So(test.element[2], ShouldEqual, 1)
			So(test.element[3], ShouldEqual, 2)
		})

		Convey("It should be able to cast it back to a float32 slice", func() {
			tmp := []float32{0, 1, 1, 2}
			test = NewFloat32Slice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, 0)
			So(tmp2[1], ShouldEqual, 1)
			So(tmp2[2], ShouldEqual, 1)
			So(tmp2[3], ShouldEqual, 2)
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewFloat32Slice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []float32{0, 1, 1, 2}
			test = NewFloat32Slice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3)
			So(test.element[4], ShouldEqual, 3)
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf(1)
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []float32{0, 1, 1, 2}
			test = NewFloat32Slice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3).Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(1)
			So(len(test.element), ShouldEqual, 2)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []float32{0, 1, 1, 2}
			test = NewFloat32Slice(tmp)
			So(test.Contains(1), ShouldBeTrue)
			So(test.Contains(3), ShouldBeFalse)
		})

	})

	Convey("Given a Float64Slice", t, func() {
		var test *Float64Slice

		Convey("It should be able to create one from a float64 slice", func() {
			tmp := []float64{0, 1, 1, 2}
			test = NewFloat64Slice(tmp)
			So(test.element[0], ShouldEqual, 0)
			So(test.element[1], ShouldEqual, 1)
			So(test.element[2], ShouldEqual, 1)
			So(test.element[3], ShouldEqual, 2)
		})

		Convey("It should be able to cast it back to a float64 slice", func() {
			tmp := []float64{0, 1, 1, 2}
			test = NewFloat64Slice(tmp)
			tmp2 := test.Cast()
			So(tmp2[0], ShouldEqual, 0)
			So(tmp2[1], ShouldEqual, 1)
			So(tmp2[2], ShouldEqual, 1)
			So(tmp2[3], ShouldEqual, 2)
		})

		Convey("It shouldn't be able to create one from nil", func() {
			test = NewFloat64Slice(nil)
			So(test, ShouldBeNil)
		})

		Convey("It should be able to append elements", func() {
			tmp := []float64{0, 1, 1, 2}
			test = NewFloat64Slice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3)
			So(test.element[4], ShouldEqual, 3)
			So(len(test.element), ShouldEqual, 5)
			test.AppendIf(1)
			So(len(test.element), ShouldEqual, 5)
		})

		Convey("It should be able to remove elements", func() {
			tmp := []float64{0, 1, 1, 2}
			test = NewFloat64Slice(tmp)
			So(len(test.element), ShouldEqual, 4)
			test.AppendIf(3).Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(3)
			So(len(test.element), ShouldEqual, 4)
			test.Remove(1)
			So(len(test.element), ShouldEqual, 2)
		})

		Convey("It should be able to check if a element is present", func() {
			tmp := []float64{0, 1, 1, 2}
			test = NewFloat64Slice(tmp)
			So(test.Contains(1), ShouldBeTrue)
			So(test.Contains(3), ShouldBeFalse)
		})

	})

}
