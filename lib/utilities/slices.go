// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package utilities

import (
	"strings"
)

//BoolSlice represents a slice of bool
type BoolSlice struct {
	element []bool
}

//NewBoolSlice creates a new bool slice
func NewBoolSlice(element []bool) *BoolSlice {
	if element == nil {
		return nil
	}
	return &BoolSlice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *BoolSlice) AppendIf(element bool) *BoolSlice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Cast converts this BoolSlice into a []bool slice
func (slice *BoolSlice) Cast() []bool {
	return slice.element
}

//Contains returns true if the slice contains the given element
func (slice *BoolSlice) Contains(element bool) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element
func (slice *BoolSlice) Remove(element bool) *BoolSlice {
	res := make([]bool, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//StringSlice represents a slice of string
type StringSlice struct {
	element []string
}

//NewStringSlice creates a new string slice
func NewStringSlice(element []string) *StringSlice {
	if element == nil {
		return nil
	}
	return &StringSlice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *StringSlice) AppendIf(element string) *StringSlice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *StringSlice) Contains(element string) bool {
	for _, value := range slice.element {
		if strings.Compare(value, element) == 0 {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *StringSlice) Remove(element string) *StringSlice {
	res := make([]string, 0, 10)
	for index := range slice.element {
		if strings.Compare(slice.element[index], element) != 0 {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this StringSlice into a []string slice
func (slice *StringSlice) Cast() []string {
	return slice.element
}

//IntSlice represents a slice of int
type IntSlice struct {
	element []int
}

//NewIntSlice creates a new int slice
func NewIntSlice(element []int) *IntSlice {
	if element == nil {
		return nil
	}
	return &IntSlice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *IntSlice) AppendIf(element int) *IntSlice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *IntSlice) Contains(element int) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *IntSlice) Remove(element int) *IntSlice {
	res := make([]int, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this IntSlice into a []int slice
func (slice *IntSlice) Cast() []int {
	return slice.element
}

//UintSlice represents a slice of uint
type UintSlice struct {
	element []uint
}

//NewUintSlice creates a new uint slice
func NewUintSlice(element []uint) *UintSlice {
	if element == nil {
		return nil
	}
	return &UintSlice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *UintSlice) AppendIf(element uint) *UintSlice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *UintSlice) Contains(element uint) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *UintSlice) Remove(element uint) *UintSlice {
	res := make([]uint, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this UintSlice into a []uint slice
func (slice *UintSlice) Cast() []uint {
	return slice.element
}

//ByteSlice represents a slice of byte
type ByteSlice struct {
	element []byte
}

//NewByteSlice creates a new byte slice
func NewByteSlice(element []byte) *ByteSlice {
	if element == nil {
		return nil
	}
	return &ByteSlice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *ByteSlice) AppendIf(element byte) *ByteSlice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *ByteSlice) Contains(element byte) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *ByteSlice) Remove(element byte) *ByteSlice {
	res := make([]byte, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this ByteSlice into a []byte slice
func (slice *ByteSlice) Cast() []byte {
	return slice.element
}

//RuneSlice represents a slice of rune
type RuneSlice struct {
	element []rune
}

//NewRuneSlice creates a new rune slice
func NewRuneSlice(element []rune) *RuneSlice {
	if element == nil {
		return nil
	}
	return &RuneSlice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *RuneSlice) AppendIf(element rune) *RuneSlice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *RuneSlice) Contains(element rune) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *RuneSlice) Remove(element rune) *RuneSlice {
	res := make([]rune, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this RuneSlice into a []rune slice
func (slice *RuneSlice) Cast() []rune {
	return slice.element
}

//Float32Slice represents a slice of float32
type Float32Slice struct {
	element []float32
}

//NewFloat32Slice creates a new float32 slice
func NewFloat32Slice(element []float32) *Float32Slice {
	if element == nil {
		return nil
	}
	return &Float32Slice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *Float32Slice) AppendIf(element float32) *Float32Slice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *Float32Slice) Contains(element float32) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *Float32Slice) Remove(element float32) *Float32Slice {
	res := make([]float32, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this Float32Slice into a []float32 slice
func (slice *Float32Slice) Cast() []float32 {
	return slice.element
}

//Float64Slice represents a slice of float64
type Float64Slice struct {
	element []float64
}

//NewFloat64Slice creates a new float64 slice
func NewFloat64Slice(element []float64) *Float64Slice {
	if element == nil {
		return nil
	}
	return &Float64Slice{element}
}

//AppendIf appends the given element if it does not exist
func (slice *Float64Slice) AppendIf(element float64) *Float64Slice {
	if slice.Contains(element) == false {
		slice.element = append(slice.element, element)
	}
	return slice
}

//Contains returns true if the slice contains the given element
func (slice *Float64Slice) Contains(element float64) bool {
	for _, value := range slice.element {
		if value == element {
			return true
		}
	}
	return false
}

//Remove removes the given element if it exists
func (slice *Float64Slice) Remove(element float64) *Float64Slice {
	res := make([]float64, 0, 10)
	for index := range slice.element {
		if slice.element[index] != element {
			res = append(res, slice.element[index])
		}
	}
	slice.element = res
	return slice
}

//Cast converts this Float64Slice into a []float64 slice
func (slice *Float64Slice) Cast() []float64 {
	return slice.element
}
