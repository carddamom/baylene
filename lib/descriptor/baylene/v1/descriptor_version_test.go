// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package v1

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTheBayleneDescriptorVersion(t *testing.T) {
	Convey("Given a BayleneDescriptorVersion", t, func() {
		version := VersionOneDotZero

		Convey("It should be able to marshal it to json", func() {
			data, err := version.MarshalJSON()
			So(err, ShouldBeNil)
			So(data, ShouldNotBeNil)
		})

		Convey("It should be able to unmarshal it from json", func() {
			data, err := version.MarshalJSON()
			So(err, ShouldBeNil)
			tmp := &BayleneDescriptorVersion{}
			err = tmp.UnmarshalJSON(data)
			So(err, ShouldBeNil)
			So(tmp.version, ShouldEqual, version.version)
		})

		Convey("It should be able to get it as String", func() {
			So(version.String(), ShouldNotBeNil)
		})
	})
}
