// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package v1

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/jzelinskie/whirlpool"
	"github.com/mailru/easyjson"
	rice "github.com/nkovacs/go.rice"
	"github.com/spf13/afero"
	schema "github.com/xeipuuv/gojsonschema"
	"gitlab.com/carddamom/baylene/lib/utilities"
	"gopkg.in/errgo.v2/fmt/errors"
)

var (
	schemaPath = "baylene-descriptor.schema.json"
	afs        *afero.Afero
)

func init() {
	afs = &afero.Afero{
		Fs: afero.NewOsFs(),
	}
}

func setAferoObject(myAfs *afero.Afero) {
	afs = myAfs
}

//ValidateDescriptor validates the given descriptor file as a valid V1 descriptor file
func ValidateDescriptor(filename string) error {
	box, err := rice.FindBox("../../../../conf")
	if err != nil {
		return errors.Because(err, err, "Validator: Unable to find schema file.")
	}

	schemaContent, err := box.String(schemaPath)
	if err != nil {
		return errors.Because(err, err, "Validator: Unable to find schema file.")
	}

	data, err := afs.ReadFile(filename)
	if err != nil {
		return errors.Because(err, err, "Validator: Unable to find json file.")
	}

	schemaLoader := schema.NewStringLoader(schemaContent)
	documentLoader := schema.NewStringLoader(string(data))

	result, err := schema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return errors.Becausef(err, err, "Validator: Unable to validate project descriptor %s.", filename)
	}
	if result.Valid() == false {
		var estr strings.Builder
		fmt.Fprintf(&estr, "Validator: Unable to validate project descriptor %s. The errors were:\n", filename)
		for _, desc := range result.Errors() {
			fmt.Fprintf(&estr, "- %s\n", desc)
		}
		return errors.New(estr.String())
	}
	return nil
}

// BayleneDescriptor describes the baylene build descriptor format version 1.0
type BayleneDescriptor struct {
	image    string                   // the docker image to use for the main build process
	services []string                 // the docker image for the services used by the build process
	steps    []BayleneDescriptorStep  // the build steps
	version  BayleneDescriptorVersion // the descriptor file version
	hash     *string                  // the descriptor file hash
}

// NewBayleneDescriptor returns a new empty baylene descriptor, with services and steps slices allocated with capacity 10
func NewBayleneDescriptor(image string) *BayleneDescriptor {
	hsh := "-1"
	result := &BayleneDescriptor{
		image:    image,
		services: make([]string, 0, 10),
		steps:    make([]BayleneDescriptorStep, 0, 10),
		version:  VersionOneDotZero,
		hash:     &hsh,
	}
	return result
}

//NewBayleneDescriptorFromJSONFile creates a new descriptor from the given file
func NewBayleneDescriptorFromJSONFile(filename string) (*BayleneDescriptor, error) {
	descriptor := &BayleneDescriptor{}

	data, err := afs.ReadFile(filename)
	if err != nil {
		return nil, errors.Becausef(err, err, "Load: Unable to read descriptor %s", filename)
	}

	err = easyjson.Unmarshal(data, descriptor)
	if err != nil {
		return nil, errors.Becausef(err, err, "Load: Unable to parse descriptor %s", filename)
	}
	if descriptor.hash == nil {
		descriptor.computeHash(data)
	}

	for i := range descriptor.Steps() {
		if descriptor.Steps()[i].UID() == nil {
			descriptor.Steps()[i].computeUUID()
		}
	}

	return descriptor, nil
}

//NewBayleneDescriptorFromJSONSlice creates a new descriptor from the given byte array (encoded in UTF-8)
func NewBayleneDescriptorFromJSONSlice(data []byte) (*BayleneDescriptor, error) {
	descriptor := &BayleneDescriptor{}
	err := easyjson.Unmarshal(data, descriptor)
	if err != nil {
		return nil, errors.Because(err, err, "Load: Unable to parse descriptor")
	}
	if descriptor.hash == nil {
		descriptor.computeHash(data)
	}

	for i := range descriptor.Steps() {
		if descriptor.Steps()[i].UID() == nil {
			descriptor.Steps()[i].computeUUID()
		}
	}

	return descriptor, nil
}

// Equal checks if the other descriptor is equal to this one
func (desc *BayleneDescriptor) Equal(other *BayleneDescriptor) bool {
	if strings.Compare(desc.image, other.Image()) != 0 {
		return false
	}
	if len(desc.services) != len(other.Services()) {
		return false
	}
	for index := range desc.services {
		if strings.Compare(desc.services[index], other.Services()[index]) != 0 {
			return false
		}
	}
	if len(desc.steps) != len(other.Steps()) {
		return false
	}
	for index := range desc.steps {
		if desc.steps[index].Equal(&other.Steps()[index]) == false {
			return false
		}
	}
	if desc.version.Equal(other.Version()) == false {
		return false
	}
	return true
}

// Image returns the docker image to use for the main build process
func (desc *BayleneDescriptor) Image() string {
	return desc.image
}

// SetImage sets the docker image to use for the main build process
func (desc *BayleneDescriptor) SetImage(image string) {
	if image != "" {
		desc.image = image
	}
}

// Version returns the descriptor file version
func (desc *BayleneDescriptor) Version() BayleneDescriptorVersion {
	return desc.version
}

// Services are the docker images for the services used by the build process
func (desc *BayleneDescriptor) Services() []string {
	return desc.services
}

// AddService adds the given service to the slice (if the service is not yet in it)
func (desc *BayleneDescriptor) AddService(service string) {
	desc.services = utilities.NewStringSlice(desc.services).AppendIf(service).Cast()
}

// RemoveService removes the given service from the slice (if the service is in it)
func (desc *BayleneDescriptor) RemoveService(service string) {
	desc.services = utilities.NewStringSlice(desc.services).Remove(service).Cast()
}

// ClearServices empties the service list
func (desc *BayleneDescriptor) ClearServices() {
	desc.services = make([]string, 0, 10)
}

// ContainsService checks the if the given service is in the slice
func (desc *BayleneDescriptor) ContainsService(service string) bool {
	return utilities.NewStringSlice(desc.services).Contains(service)
}

// Steps are the build steps
func (desc *BayleneDescriptor) Steps() []BayleneDescriptorStep {
	return desc.steps
}

// AddStep adds the given step to this descriptor (if the step is not yet in it)
func (desc *BayleneDescriptor) AddStep(step *BayleneDescriptorStep) {
	for _, value := range desc.steps {
		if value.Equal(step) == true {
			return
		}
	}
	desc.steps = append(desc.steps, *step)
}

// ContainsStep checks if the slice contains the given step
func (desc *BayleneDescriptor) ContainsStep(step *BayleneDescriptorStep) bool {
	for _, value := range desc.steps {
		if value.Equal(step) == true {
			return true
		}
	}
	return false
}

// RemoveStep removes the given step from the slide (if the step is in it)
func (desc *BayleneDescriptor) RemoveStep(step *BayleneDescriptorStep) {
	tmp := make([]BayleneDescriptorStep, 0, 10)
	for _, value := range desc.steps {
		if value.Equal(step) == false {
			tmp = append(tmp, value)
		}
	}
	desc.steps = tmp
}

// ClearSteps empties the step list
func (desc *BayleneDescriptor) ClearSteps() {
	desc.steps = make([]BayleneDescriptorStep, 0, 10)
}

// Hash is the descriptor file hash
func (desc *BayleneDescriptor) Hash() *string {
	return desc.hash
}

func (desc *BayleneDescriptor) computeHash(content []byte) {
	h := whirlpool.New()
	h.Write(content)
	val := base64.StdEncoding.EncodeToString(h.Sum(nil))
	desc.hash = &val
}

// StoreInJSONFile saves the current descriptor into the given file
func (desc *BayleneDescriptor) StoreInJSONFile(filename string, overwrite bool) error {
	exists, err := afs.Exists(filename)
	if err != nil {
		return errors.Becausef(err, err, "Save: Unable to store descriptor to json %s", filename)
	}
	if (exists == true) && (overwrite == false) {
		return errors.Becausef(err, err, "Save: Unable to store descriptor to json %s, file exists", filename)
	}

	data, err := easyjson.Marshal(desc)
	if err != nil {
		return errors.Becausef(err, err, "Save: Unable to store descriptor to json %s", filename)
	}

	err = afs.WriteFile(filename, data, 0644)
	if err != nil {
		return errors.Becausef(err, err, "Save: Unable to store descriptor to json %s", filename)
	}

	return nil
}

//StoreInJSONSlice saves the current descriptor to the given byte slice
func (desc *BayleneDescriptor) StoreInJSONSlice() ([]byte, error) {
	data, err := easyjson.Marshal(desc)
	if err != nil {
		return nil, errors.Because(err, err, "Save: Unable to store descriptor to json slice")
	}
	return data, nil
}
