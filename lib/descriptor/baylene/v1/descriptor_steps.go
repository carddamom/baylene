// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package v1

import (
	easyjson "github.com/mailru/easyjson"
	uuid "github.com/satori/go.uuid"
	utilities "gitlab.com/carddamom/baylene/lib/utilities"
	errors "gopkg.in/errgo.v2/fmt/errors"
)

// BayleneDescriptorStep corresponds to a build step
type BayleneDescriptorStep struct {
	cmd          []string          // the step commands
	dependencies []string          // the step dependencies (between other steps)
	name         string            // the step name
	options      map[string]string // step options
	uid          *string           // step uuid
}

// NewBayleneDescriptorStep creates a new baylene descriptor step
func NewBayleneDescriptorStep(name string) *BayleneDescriptorStep {
	result := &BayleneDescriptorStep{
		cmd:          make([]string, 0, 10),
		dependencies: make([]string, 0, 10),
		name:         name,
		options:      make(map[string]string),
	}
	result.computeUUID()
	return result
}

// NewBayleneDescriptorStepFromJSONFile creates a new descriptor step from the given file
func NewBayleneDescriptorStepFromJSONFile(filename string) (*BayleneDescriptorStep, error) {
	step := &BayleneDescriptorStep{}

	data, err := afs.ReadFile(filename)
	if err != nil {
		return nil, errors.Becausef(err, err, "Load: Unable to read descriptor step %s", filename)
	}

	err = easyjson.Unmarshal(data, step)
	if err != nil {
		return nil, errors.Becausef(err, err, "Load: Unable to parse descriptorstep  %s", filename)
	}

	if step.uid == nil {
		step.computeUUID()
	}

	return step, nil
}

//NewBayleneDescriptorStepFromJSONSlice creates a new step from the given byte array (encoded in UTF-8)
func NewBayleneDescriptorStepFromJSONSlice(data []byte) (*BayleneDescriptorStep, error) {
	step := &BayleneDescriptorStep{}
	err := easyjson.Unmarshal(data, step)
	if err != nil {
		return nil, errors.Because(err, err, "Load: Unable to parse descriptor step")
	}
	if step.uid == nil {
		step.computeUUID()
	}

	return step, nil
}

// Equal checks if two descriptor steps are equal
func (step *BayleneDescriptorStep) Equal(other *BayleneDescriptorStep) bool {
	if step.name != other.Name() {
		return false
	}
	if len(step.cmd) != len(other.Cmd()) {
		return false
	}
	for index := range step.cmd {
		if step.cmd[index] != other.Cmd()[index] {
			return false
		}
	}
	if len(step.dependencies) != len(other.Dependencies()) {
		return false
	}
	for index := range step.dependencies {
		if step.dependencies[index] != other.Dependencies()[index] {
			return false
		}
	}
	if len(step.options) != len(other.Options()) {
		return false
	}
	for key := range step.options {
		me, meExists := step.options[key]
		other, otherExists := other.Options()[key]
		if (meExists == false) || (otherExists == false) {
			return false
		}
		if me != other {
			return false
		}
	}
	return true
}

// Cmd are the step commands
func (step *BayleneDescriptorStep) Cmd() []string {
	return step.cmd
}

// AddCmd adds the given cmd to the slice (if the cmd is not yet in it)
func (step *BayleneDescriptorStep) AddCmd(cmd string) {
	step.cmd = utilities.NewStringSlice(step.cmd).AppendIf(cmd).Cast()
}

// RemoveCmd removes the given cmd from the slide (if the cmd is in it)
func (step *BayleneDescriptorStep) RemoveCmd(cmd string) {
	step.cmd = utilities.NewStringSlice(step.cmd).Remove(cmd).Cast()
}

// ClearCmd empties the cmd list
func (step *BayleneDescriptorStep) ClearCmd() {
	step.cmd = make([]string, 0, 10)
}

// ContainsCmd checks if the the given command is a command of this step
func (step *BayleneDescriptorStep) ContainsCmd(command string) bool {
	return utilities.NewStringSlice(step.cmd).Contains(command)
}

// Dependencies are the step dependencies (between other steps)
func (step *BayleneDescriptorStep) Dependencies() []string {
	return step.dependencies
}

// AddDependency adds the given dependency to the slice (if the dependency is not yet in it)
func (step *BayleneDescriptorStep) AddDependency(dependency string) {
	step.dependencies = utilities.NewStringSlice(step.dependencies).AppendIf(dependency).Cast()
}

// RemoveDependency removes the given dependency from the slide (if the dependency is in it)
func (step *BayleneDescriptorStep) RemoveDependency(dependency string) {
	step.dependencies = utilities.NewStringSlice(step.dependencies).Remove(dependency).Cast()
}

// ClearDependencies empties the dependency list
func (step *BayleneDescriptorStep) ClearDependencies() {
	step.dependencies = make([]string, 0, 10)
}

// ContainsDependency checks if the the given dependency is a dependency of this step
func (step *BayleneDescriptorStep) ContainsDependency(dependency string) bool {
	return utilities.NewStringSlice(step.dependencies).Contains(dependency)
}

// Name is the step name
func (step *BayleneDescriptorStep) Name() string {
	return step.name
}

// Options are the step options
func (step *BayleneDescriptorStep) Options() map[string]string {
	return step.options
}

// AddOption adds the given option to this step
func (step *BayleneDescriptorStep) AddOption(key, value string) {
	_, exists := step.options[key]
	if exists == false {
		step.options[key] = value
	}
}

// RemoveOption removes the given option from this step
func (step *BayleneDescriptorStep) RemoveOption(key string) {
	_, exists := step.options[key]
	if exists == true {
		delete(step.options, key)
	}
}

// GetOption returns the given option from this step if it exists
func (step *BayleneDescriptorStep) GetOption(key string) (string, bool) {
	val, exists := step.options[key]
	return val, exists
}

// UID is the step uuid
func (step *BayleneDescriptorStep) UID() *string {
	return step.uid
}

func (step *BayleneDescriptorStep) computeUUID() {
	uid := uuid.NewV5(uuid.NewV1(), step.name)
	id := uid.String()
	step.uid = &id
}

// StoreInJSONFile stores this descriptor step in the given json file
func (step *BayleneDescriptorStep) StoreInJSONFile(filename string, overwrite bool) error {
	exists, err := afs.Exists(filename)
	if err != nil {
		return errors.Becausef(err, err, "Save: Unable to store descriptor step to json %s", filename)
	}
	if (exists == true) && (overwrite == false) {
		return errors.Becausef(err, err, "Save: Unable to store descriptor step to json %s, file exists", filename)
	}

	data, err := easyjson.Marshal(step)
	if err != nil {
		return errors.Becausef(err, err, "Save: Unable to store descriptor step to json %s", filename)
	}

	err = afs.WriteFile(filename, data, 0644)
	if err != nil {
		return errors.Becausef(err, err, "Save: Unable to store descriptor step to json %s", filename)
	}

	return nil
}

//StoreInJSONSlice stores the current descriptor to the given byte slice
func (step *BayleneDescriptorStep) StoreInJSONSlice() ([]byte, error) {
	data, err := easyjson.Marshal(step)
	if err != nil {
		return nil, errors.Because(err, err, "Save: Unable to store descriptor step to json slice")
	}
	return data, nil
}
