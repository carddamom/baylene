// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package v1

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/afero"
)

func TestBayleneDescriptors(t *testing.T) {
	Convey("Given a Baylene descriptor", t, func() {
		descriptor := NewBayleneDescriptor("testing")
		setAferoObject(&afero.Afero{
			Fs: afero.NewMemMapFs(),
		})
		Convey("It should be able to save it to a json byte slice", func() {
			descriptor = NewBayleneDescriptor("testing")
			data, err := descriptor.StoreInJSONSlice()
			descriptor.computeHash(data)
			So(err, ShouldBeNil)
			So(data, ShouldNotBeNil)
			So(data, ShouldNotBeEmpty)
			So(descriptor.hash, ShouldNotBeNil)
			So(*descriptor.hash, ShouldNotBeEmpty)
		})
		Convey("It should be able to load it from a json byte slice", func() {
			descriptor = NewBayleneDescriptor("testing")
			data, err := descriptor.StoreInJSONSlice()
			So(err, ShouldBeNil)
			So(data, ShouldNotBeNil)
			So(data, ShouldNotBeEmpty)
			tmp, err := NewBayleneDescriptorFromJSONSlice(data)
			So(err, ShouldBeNil)
			So(tmp.Image(), ShouldEqual, descriptor.Image())
			So(*tmp.Hash(), ShouldEqual, *descriptor.Hash())
			tmp, err = NewBayleneDescriptorFromJSONSlice([]byte("spam, bacon, eggs, spam, spam, spam"))
			So(err, ShouldNotBeNil)
			So(tmp, ShouldBeNil)
		})
		Convey("It should be able to save it to a json file", func() {
			afer := &afero.Afero{
				Fs: afero.NewMemMapFs(),
			}
			setAferoObject(afer)
			descriptor = NewBayleneDescriptor("testing")
			err := descriptor.StoreInJSONFile("descriptor.json", true)
			So(err, ShouldBeNil)
			exists, err := afer.Exists("descriptor.json")
			So(err, ShouldBeNil)
			So(exists, ShouldBeTrue)
			err = descriptor.StoreInJSONFile("descriptor.json", false)
			So(err, ShouldNotBeNil)
		})
		Convey("It should be able to load it from a json file", func() {
			afer := &afero.Afero{
				Fs: afero.NewMemMapFs(),
			}
			setAferoObject(afer)
			descriptor = NewBayleneDescriptor("testing")
			err := descriptor.StoreInJSONFile("descriptor.json", true)
			So(err, ShouldBeNil)
			tmp, err := NewBayleneDescriptorFromJSONFile("descriptor.json")
			So(err, ShouldBeNil)
			So(tmp.Image(), ShouldEqual, descriptor.Image())
			So(*tmp.Hash(), ShouldEqual, *descriptor.Hash())
			tmp, err = NewBayleneDescriptorFromJSONFile("inexistant.json")
			So(err, ShouldNotBeNil)
			So(tmp, ShouldBeNil)
			afer.WriteFile("randomfile.json", []byte("spam, bacon, eggs, spam, spam, spam"), 0644)
			tmp, err = NewBayleneDescriptorFromJSONFile("randomfile.json")
			So(err, ShouldNotBeNil)
			So(tmp, ShouldBeNil)
		})
		Convey("It should be able to get the image of a descriptor", func() {
			descriptor = NewBayleneDescriptor("testing")
			So(descriptor.image, ShouldEqual, "testing")
			So(descriptor.Image(), ShouldEqual, "testing")
		})
		Convey("It should be able to add a service", func() {
			descriptor = NewBayleneDescriptor("testing")
			descriptor.AddService("command")
			So(len(descriptor.services), ShouldEqual, 1)
			So(descriptor.services[0], ShouldEqual, "command")
			So(len(descriptor.Services()), ShouldEqual, 1)
			So(descriptor.Services()[0], ShouldEqual, "command")
			descriptor.AddService("command")
			So(len(descriptor.services), ShouldEqual, 1)
			So(len(descriptor.Services()), ShouldEqual, 1)
		})
		Convey("It should be able to remove a service", func() {
			descriptor = NewBayleneDescriptor("testing")
			descriptor.AddService("command")
			So(len(descriptor.services), ShouldEqual, 1)
			So(len(descriptor.Services()), ShouldEqual, 1)
			descriptor.RemoveService("command")
			So(len(descriptor.services), ShouldEqual, 0)
			So(len(descriptor.Services()), ShouldEqual, 0)
			descriptor.AddService("command")
			So(len(descriptor.services), ShouldEqual, 1)
			So(len(descriptor.Services()), ShouldEqual, 1)
			descriptor.ClearServices()
			So(len(descriptor.services), ShouldEqual, 0)
			So(len(descriptor.Services()), ShouldEqual, 0)
		})
		Convey("It should be able to see if a descriptor contains a service", func() {
			descriptor = NewBayleneDescriptor("testing")
			descriptor.AddService("command")
			So(len(descriptor.services), ShouldEqual, 1)
			So(len(descriptor.Services()), ShouldEqual, 1)
			So(descriptor.ContainsService("command"), ShouldBeTrue)
			So(descriptor.ContainsService("bacon"), ShouldBeFalse)
		})
		Convey("It should be able to add a step", func() {
			descriptor = NewBayleneDescriptor("testing")
			step := NewBayleneDescriptorStep("testing")
			descriptor.AddStep(step)
			So(len(descriptor.steps), ShouldEqual, 1)
			So(len(descriptor.Steps()), ShouldEqual, 1)
			descriptor.AddStep(step)
			So(len(descriptor.steps), ShouldEqual, 1)
			So(len(descriptor.Steps()), ShouldEqual, 1)
		})
		Convey("It should be able to remove a step", func() {
			descriptor = NewBayleneDescriptor("testing")
			step := NewBayleneDescriptorStep("testing")
			descriptor.AddStep(step)
			So(len(descriptor.steps), ShouldEqual, 1)
			So(len(descriptor.Steps()), ShouldEqual, 1)
			descriptor.RemoveStep(step)
			So(len(descriptor.steps), ShouldEqual, 0)
			So(len(descriptor.Steps()), ShouldEqual, 0)
			descriptor.AddStep(step)
			So(len(descriptor.steps), ShouldEqual, 1)
			So(len(descriptor.Steps()), ShouldEqual, 1)
			descriptor.ClearSteps()
			So(len(descriptor.steps), ShouldEqual, 0)
			So(len(descriptor.Steps()), ShouldEqual, 0)
			descriptor.AddStep(step)
			step = NewBayleneDescriptorStep("bacon")
			descriptor.AddStep(step)
			So(len(descriptor.steps), ShouldEqual, 2)
			So(len(descriptor.Steps()), ShouldEqual, 2)
			descriptor.RemoveStep(step)
			So(len(descriptor.steps), ShouldEqual, 1)
			So(len(descriptor.Steps()), ShouldEqual, 1)
		})
		Convey("It should be able to see if a descriptor contains a step", func() {
			descriptor = NewBayleneDescriptor("testing")
			step := NewBayleneDescriptorStep("testing")
			descriptor.AddStep(step)
			So(len(descriptor.steps), ShouldEqual, 1)
			So(len(descriptor.Steps()), ShouldEqual, 1)
			So(descriptor.ContainsStep(step), ShouldBeTrue)
			step = NewBayleneDescriptorStep("bacon")
			So(descriptor.ContainsStep(step), ShouldBeFalse)
		})
		Convey("It should be able to get the version of a descriptor", func() {
			descriptor = NewBayleneDescriptor("testing")
			ver := descriptor.Version()
			So(descriptor.version.String(), ShouldEqual, VersionOneDotZero.String())
			So(ver.String(), ShouldEqual, VersionOneDotZero.String())
		})
		Convey("It should be able to check another descriptor for equality", func() {
			descriptor = NewBayleneDescriptor("testing")
			descriptor1 := NewBayleneDescriptor("testing")
			So(descriptor.Equal(descriptor1), ShouldBeTrue)

			descriptor1 = NewBayleneDescriptor("testing")
			descriptor1.SetImage("spam")
			So(descriptor.Equal(descriptor1), ShouldBeFalse)

			descriptor1 = NewBayleneDescriptor("testing")
			descriptor1.AddService("spam")
			So(descriptor.Equal(descriptor1), ShouldBeFalse)

			descriptor1 = NewBayleneDescriptor("testing")
			descriptor1.AddStep(NewBayleneDescriptorStep("spam"))
			So(descriptor.Equal(descriptor1), ShouldBeFalse)

			descriptor = NewBayleneDescriptor("testing")
			descriptor.AddService("eggs")
			descriptor1 = NewBayleneDescriptor("testing")
			descriptor1.AddService("spam")
			So(descriptor.Equal(descriptor1), ShouldBeFalse)

			descriptor = NewBayleneDescriptor("testing")
			descriptor.AddStep(NewBayleneDescriptorStep("eggs"))
			descriptor1 = NewBayleneDescriptor("testing")
			descriptor1.AddStep(NewBayleneDescriptorStep("spam"))
			So(descriptor.Equal(descriptor1), ShouldBeFalse)
		})
	})

	Convey("Given a json file", t, func() {
		descriptor := NewBayleneDescriptor("testing")
		afer := &afero.Afero{
			Fs: afero.NewMemMapFs(),
		}
		setAferoObject(afer)
		descriptor.hash = nil
		err := descriptor.StoreInJSONFile("descriptor.json", true)
		So(err, ShouldBeNil)
		exists, err := afer.Exists("descriptor.json")
		So(err, ShouldBeNil)
		So(exists, ShouldBeTrue)
		Convey("It should be possible to validate it", func() {
			err := ValidateDescriptor("descriptor.json")
			So(err, ShouldBeNil)
		})
		Convey("It should not be possible to validate it if it does not exist", func() {
			err := ValidateDescriptor("fakedescriptor.json")
			So(err, ShouldNotBeNil)
		})
		Convey("It should not be possible to validate it if it is invalid", func() {
			afer.WriteFile("randomfile.json", []byte("spam, bacon, eggs, spam, spam, spam"), 0644)
			err := ValidateDescriptor("randomfile.json")
			So(err, ShouldNotBeNil)
			descriptor := NewBayleneDescriptor("testing")
			err = descriptor.StoreInJSONFile("descriptor.json", true)
			So(err, ShouldBeNil)
			err = ValidateDescriptor("descriptor.json")
			So(err, ShouldNotBeNil)
		})
	})
}
