// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package v1

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/afero"
)

func TestTheBayleneDescriptorStep(t *testing.T) {
	Convey("Given a Baylene Descriptor Step", t, func() {
		step := NewBayleneDescriptorStep("testing")
		setAferoObject(&afero.Afero{
			Fs: afero.NewMemMapFs(),
		})
		Convey("It should be able to marshal it to a json byte slice", func() {
			step = NewBayleneDescriptorStep("testing")
			data, err := step.StoreInJSONSlice()
			So(err, ShouldBeNil)
			So(data, ShouldNotBeNil)
			So(data, ShouldNotBeEmpty)
		})
		Convey("It should be able to unmarshal it from a json byte slice", func() {
			step = NewBayleneDescriptorStep("testing")
			data, err := step.StoreInJSONSlice()
			So(err, ShouldBeNil)
			So(data, ShouldNotBeNil)
			So(data, ShouldNotBeEmpty)
			tmp, err := NewBayleneDescriptorStepFromJSONSlice(data)
			So(err, ShouldBeNil)
			So(tmp.Name(), ShouldEqual, step.Name())
			So(*tmp.UID(), ShouldEqual, *step.UID())
			tmp, err = NewBayleneDescriptorStepFromJSONSlice([]byte("spam, bacon, eggs, spam, spam, spam"))
			So(err, ShouldNotBeNil)
			So(tmp, ShouldBeNil)
		})
		Convey("It should be able to marshal it to a json file", func() {
			afer := &afero.Afero{
				Fs: afero.NewMemMapFs(),
			}
			setAferoObject(afer)
			step = NewBayleneDescriptorStep("testing")
			err := step.StoreInJSONFile("descriptor.json", true)
			So(err, ShouldBeNil)
			exists, err := afer.Exists("descriptor.json")
			So(err, ShouldBeNil)
			So(exists, ShouldBeTrue)
			err = step.StoreInJSONFile("descriptor.json", false)
			So(err, ShouldNotBeNil)
		})
		Convey("It should be able to unmarshal it from a json file", func() {
			afer := &afero.Afero{
				Fs: afero.NewMemMapFs(),
			}
			setAferoObject(afer)
			step = NewBayleneDescriptorStep("testing")
			err := step.StoreInJSONFile("descriptor.json", true)
			So(err, ShouldBeNil)
			tmp, err := NewBayleneDescriptorStepFromJSONFile("descriptor.json")
			So(err, ShouldBeNil)
			So(tmp.Name(), ShouldEqual, step.Name())
			So(*tmp.UID(), ShouldEqual, *step.UID())
			tmp, err = NewBayleneDescriptorStepFromJSONFile("inexistant.json")
			So(err, ShouldNotBeNil)
			So(tmp, ShouldBeNil)
			afer.WriteFile("randomfile.json", []byte("spam, bacon, eggs, spam, spam, spam"), 0644)
			tmp, err = NewBayleneDescriptorStepFromJSONFile("randomfile.json")
			So(err, ShouldNotBeNil)
			So(tmp, ShouldBeNil)
		})
		Convey("It should be able to add a new command", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddCmd("command")
			So(len(step.cmd), ShouldEqual, 1)
			So(step.cmd[0], ShouldEqual, "command")
			So(len(step.Cmd()), ShouldEqual, 1)
			So(step.Cmd()[0], ShouldEqual, "command")
		})
		Convey("It should be able to remove a command", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddCmd("command")
			So(len(step.cmd), ShouldEqual, 1)
			So(len(step.Cmd()), ShouldEqual, 1)
			step.RemoveCmd("command")
			So(len(step.cmd), ShouldEqual, 0)
			So(len(step.Cmd()), ShouldEqual, 0)
			step.AddCmd("command")
			So(len(step.cmd), ShouldEqual, 1)
			So(len(step.Cmd()), ShouldEqual, 1)
			step.ClearCmd()
			So(len(step.cmd), ShouldEqual, 0)
			So(len(step.Cmd()), ShouldEqual, 0)
		})
		Convey("It should be able to check a command for existance", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddCmd("command")
			So(len(step.cmd), ShouldEqual, 1)
			So(len(step.Cmd()), ShouldEqual, 1)
			So(step.ContainsCmd("command"), ShouldBeTrue)
		})
		Convey("It should be able to add a new dependency", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddDependency("dependency")
			So(len(step.dependencies), ShouldEqual, 1)
			So(step.dependencies[0], ShouldEqual, "dependency")
			So(len(step.Dependencies()), ShouldEqual, 1)
			So(step.Dependencies()[0], ShouldEqual, "dependency")
		})
		Convey("It should be able to remove a dependency", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddDependency("dependency")
			So(len(step.dependencies), ShouldEqual, 1)
			So(len(step.Dependencies()), ShouldEqual, 1)
			step.RemoveDependency("dependency")
			So(len(step.dependencies), ShouldEqual, 0)
			So(len(step.Dependencies()), ShouldEqual, 0)
			step.AddDependency("dependency")
			So(len(step.dependencies), ShouldEqual, 1)
			So(len(step.Dependencies()), ShouldEqual, 1)
			step.ClearDependencies()
			So(len(step.dependencies), ShouldEqual, 0)
			So(len(step.Dependencies()), ShouldEqual, 0)
		})
		Convey("It should be able to check a dependency for existance", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddDependency("dependency")
			So(len(step.dependencies), ShouldEqual, 1)
			So(len(step.Dependencies()), ShouldEqual, 1)
			So(step.ContainsDependency("dependency"), ShouldBeTrue)
		})
		Convey("It should be able to add a new option", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddOption("testing", "true")
			So(len(step.options), ShouldEqual, 1)
			So(len(step.Options()), ShouldEqual, 1)
			step.AddOption("testing", "true")
			So(len(step.options), ShouldEqual, 1)
			So(len(step.Options()), ShouldEqual, 1)
		})
		Convey("It should be able to remove a option", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddOption("testing", "true")
			So(len(step.options), ShouldEqual, 1)
			So(len(step.Options()), ShouldEqual, 1)
			step.RemoveOption("testing")
			So(len(step.options), ShouldEqual, 0)
			So(len(step.Options()), ShouldEqual, 0)
		})
		Convey("It should be able to get a option", func() {
			step = NewBayleneDescriptorStep("testing")
			step.AddOption("testing", "true")
			So(len(step.options), ShouldEqual, 1)
			So(len(step.Options()), ShouldEqual, 1)
			option, exists := step.GetOption("testing")
			So(exists, ShouldBeTrue)
			So(option, ShouldEqual, "true")
			option, exists = step.GetOption("bacon")
			So(exists, ShouldBeFalse)
			So(option, ShouldBeEmpty)
		})
		Convey("It should be able to get the uuid of the step", func() {
			step = NewBayleneDescriptorStep("testing")
			So(step.UID(), ShouldNotBeNil)
			So(*step.UID(), ShouldNotBeEmpty)
		})
		Convey("It should be able to check a step for equality", func() {
			step = NewBayleneDescriptorStep("testing")
			step1 := NewBayleneDescriptorStep("testing")
			So(step.Equal(step1), ShouldBeTrue)

			step1.name = "spam"
			So(step.Equal(step1), ShouldBeFalse)

			step1 = NewBayleneDescriptorStep("testing")
			step1.AddCmd("spam")
			So(step.Equal(step1), ShouldBeFalse)

			step1 = NewBayleneDescriptorStep("testing")
			step1.AddDependency("spam")
			So(step.Equal(step1), ShouldBeFalse)

			step1 = NewBayleneDescriptorStep("testing")
			step1.AddOption("spam", "true")
			So(step.Equal(step1), ShouldBeFalse)

			step = NewBayleneDescriptorStep("testing")
			step.AddCmd("egg")
			step1 = NewBayleneDescriptorStep("testing")
			step1.AddCmd("spam")
			So(step.Equal(step1), ShouldBeFalse)

			step = NewBayleneDescriptorStep("testing")
			step.AddDependency("egg")
			step1 = NewBayleneDescriptorStep("testing")
			step1.AddDependency("spam")
			So(step.Equal(step1), ShouldBeFalse)

			step = NewBayleneDescriptorStep("testing")
			step.AddOption("egg", "true")
			step1 = NewBayleneDescriptorStep("testing")
			step1.AddOption("spam", "true")
			So(step.Equal(step1), ShouldBeFalse)
		})
	})
}
