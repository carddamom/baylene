// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package internal

import (
	"gitlab.com/carddamom/baylene/lib/storage"
	"gopkg.in/errgo.v2/fmt/errors"
)

// BadgerStore is a store implemented using the badger key/value database
type BadgerStore struct {
}

//Open the storage in the given location
func (store *BadgerStore) Open(location string) error {
	return errors.New("Operation not implemented")
}

//Close the storage
func (store *BadgerStore) Close() error {
	return errors.New("Operation not implemented")
}

//ReadDescriptor reads the descriptor with the given name from storage
func (store *BadgerStore) ReadDescriptor(name string) (*storage.Descriptor, error) {
	return nil, errors.New("Operation not implemented")
}

//WriteDescriptor writes the given descriptor and name to the storage
func (store *BadgerStore) WriteDescriptor(name string, descriptor *storage.Descriptor) error {
	return errors.New("Operation not implemented")
}

//ListDescriptors returns the available descriptor names
func (store *BadgerStore) ListDescriptors() ([]string, error) {
	return nil, errors.New("Operation not implemented")
}
