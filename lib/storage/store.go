// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package storage

// Storage is the interface that defines a descriptor storage
type Storage interface {
	Open(location string) error                                // Opens the storage in the given location
	Close() error                                              // Closes the storage
	ReadDescriptor(name string) (*Descriptor, error)           // Reads the descriptor with the given name from storage
	WriteDescriptor(name string, descriptor *Descriptor) error //Writes the given descriptor and name to the storage
	ListDescriptors() (*string, error)                         // Lists the available descriptor names
}

// Descriptor represents a versionless descriptor, that can store all descriptor
// information irrelevant of its version
type Descriptor interface {
	Name() string
	Hash() string
	Image() string
	Services() []string
	Steps() []DescriptorStep
	Store() ([]byte, error)
	Load([]byte) error
}

// DescriptorStep is a build step
type DescriptorStep interface {
	Cmd() []string
	Dependencies() []string
	Name() string
	ID() string
	Options() map[string]string
	Store() ([]byte, error)
	Load([]byte) error
}
