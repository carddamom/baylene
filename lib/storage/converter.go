// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package storage

import (
	v1descriptor "gitlab.com/carddamom/baylene/lib/descriptor/baylene/v1"
	errors "gopkg.in/errgo.v2/fmt/errors"
)

// Converter is a converter function that takes a specific descriptor and converts it into the internal format
type Converter func(origin interface{}) (*Descriptor, error)

// BayleneDescriptorV1Converter is a converter from the baylene descriptor version 1 into the internal version
func BayleneDescriptorV1Converter(origin interface{}) (Descriptor, error) {
	desc, ok := origin.(v1descriptor.BayleneDescriptor)
	if ok == false {
		return nil, errors.Newf("Converter: Unable to converter the given descriptor.")
	}
	dest := &DescriptorSimple{}
	dest.IName = ""
	if desc.Hash() != nil {
		dest.IHash = *desc.Hash()
	}

	dest.IImage = desc.Image()

	for _, service := range desc.Services() {
		dest.IServices = append(dest.IServices, service)
	}

	for _, step := range desc.Steps() {
		dest.ISteps = append(dest.ISteps, convertStepFromV1(step))
	}
	return dest, nil
}

func convertStepFromV1(step v1descriptor.BayleneDescriptorStep) *DescriptorStepSimple {
	dest := &DescriptorStepSimple{}

	for _, command := range step.Cmd() {
		dest.ICmd = append(dest.ICmd, command)
	}

	for _, dependency := range step.Dependencies() {
		dest.IDependencies = append(dest.IDependencies, dependency)
	}

	dest.IName = step.Name()

	for key, value := range step.Options() {
		dest.IOptions[key] = value
	}

	if step.UID() != nil {
		dest.IID = *step.UID()
	}

	return dest
}
