// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package serializer

import (
	"github.com/homedepot/mgo/bson"
	"gitlab.com/carddamom/baylene/lib/storage"
	"gitlab.com/carddamom/baylene/lib/utilities"
	"gopkg.in/errgo.v2/fmt/errors"
)

// DescriptorBSON is a concrete descriptor that serializes to BSON
type DescriptorBSON struct {
	name     string                // the name of the project
	hash     string                // the hash of the descriptor file
	image    string                // the docker image to use for the main build process
	services []string              // the docker image for the services used by the build process
	steps    []*DescriptorStepBSON // the build steps by topological order (ie. ready to be executed)
}

// CopyToDescriptorBSON creates a new BSON descriptor from the given origin
func CopyToDescriptorBSON(origin storage.Descriptor) *DescriptorBSON {
	descriptor := &DescriptorBSON{
		name:     origin.Name(),
		hash:     origin.Hash(),
		image:    origin.Image(),
		services: make([]string, 0, len(origin.Services())),
		steps:    make([]*DescriptorStepBSON, 0, len(origin.Steps())),
	}

	srvs := utilities.NewStringSlice(descriptor.services)
	for _, v := range origin.Services() {
		srvs.AppendIf(v)
	}
	descriptor.services = srvs.Cast()

	for _, v := range origin.Steps() {
		descriptor.steps = append(descriptor.steps, CopyToDescriptorStepBSON(v))
	}

	return descriptor
}

// NewDescriptorBSON creates a new BSON descriptor
func NewDescriptorBSON(name, image string) *DescriptorBSON {
	return &DescriptorBSON{
		name:     name,
		hash:     "-1",
		image:    image,
		services: make([]string, 0, 10),
		steps:    make([]*DescriptorStepBSON, 0, 10),
	}
}

// Name is the name of the project
func (project *DescriptorBSON) Name() string {
	return project.name
}

// Hash is the hash of the descriptor file
func (project *DescriptorBSON) Hash() string {
	return project.hash
}

// Image is the docker image to use for the main build process
func (project *DescriptorBSON) Image() string {
	return project.image
}

// Services are the docker images for the services used by the build process
func (project *DescriptorBSON) Services() []string {
	return project.services
}

// Steps are the build steps
func (project *DescriptorBSON) Steps() []storage.DescriptorStep {
	steps := make([]storage.DescriptorStep, len(project.steps))
	for i, v := range project.steps {
		steps[i] = storage.DescriptorStep(v)
	}
	return steps
}

// Store serializes this descriptor (operation not implemented)
func (project *DescriptorBSON) Store() ([]byte, error) {
	dest := &descriptorBSONInternal{
		Name:     project.name,
		Hash:     project.hash,
		Image:    project.image,
		Services: project.services,
		Steps:    make([]*descriptorStepBSONInternal, 0, len(project.steps)),
	}

	for _, v := range project.steps {
		dest.Steps = append(dest.Steps, &descriptorStepBSONInternal{
			Cmd:          v.cmd,
			Dependencies: v.dependencies,
			Name:         v.name,
			Options:      v.options,
			ID:           v.id,
		})
	}

	data, err := bson.Marshal(dest)
	return data, errors.Because(err, err, "Store: Unable to serialize this descriptor")
}

// Load deserializes the given byte slice into a descriptor (operation not implemented)
func (project *DescriptorBSON) Load(data []byte) error {
	dest := &descriptorBSONInternal{}

	err := bson.Unmarshal(data, dest)
	if err != nil {
		return errors.Because(err, err, "Store: Unable to serialize this descriptor")
	}

	project.loadFrom( dest )

	return nil
}

func (project *DescriptorBSON) loadFrom(other *descriptorBSONInternal) {
	srvs := utilities.NewStringSlice(project.services)
	for _, v := range other.Services {
		srvs.AppendIf(v)
	}
	project.services = srvs.Cast()

	var step *DescriptorStepBSON
	for _, v := range other.Steps {
		step = NewDescriptorStepBSON(v.Name)
		step.loadFrom(v)
		project.steps = append(project.steps, step)
	}

	project.name = other.Name
	project.image = other.Image
	project.hash = other.Hash
}

//DescriptorStepBSON is a build step
type DescriptorStepBSON struct {
	cmd          []string          // the step commands
	dependencies []string          // the step dependencies (between other steps)
	name         string            // the step name
	options      map[string]string // step options
	id           string            // the step uuid
}

// CopyToDescriptorStepBSON creates a new BSON descriptor step from the given origin
func CopyToDescriptorStepBSON(origin storage.DescriptorStep) *DescriptorStepBSON {
	step := &DescriptorStepBSON{
		cmd:          make([]string, 0, len(origin.Cmd())),
		dependencies: make([]string, 0, len(origin.Dependencies())),
		name:         origin.Name(),
		options:      make(map[string]string),
		id:           origin.ID(),
	}

	srvs := utilities.NewStringSlice(step.cmd)
	for _, v := range origin.Cmd() {
		srvs.AppendIf(v)
	}
	step.cmd = srvs.Cast()

	srvs = utilities.NewStringSlice(step.dependencies)
	for _, v := range origin.Dependencies() {
		srvs.AppendIf(v)
	}
	step.dependencies = srvs.Cast()

	for key, value := range origin.Options() {
		step.options[key] = value
	}

	return step
}

// NewDescriptorStepBSON creates a new BSON descriptor step
func NewDescriptorStepBSON(name string) *DescriptorStepBSON {
	return &DescriptorStepBSON{
		cmd:          make([]string, 0, 10),
		dependencies: make([]string, 0, 10),
		name:         name,
		options:      make(map[string]string),
		id:           "-1",
	}
}

// Cmd are the step commands
func (step *DescriptorStepBSON) Cmd() []string {
	return step.cmd
}

// Dependencies are the step dependencies
func (step *DescriptorStepBSON) Dependencies() []string {
	return step.dependencies
}

// Name is the step name
func (step *DescriptorStepBSON) Name() string {
	return step.name
}

// ID is the step uuid
func (step *DescriptorStepBSON) ID() string {
	return step.id
}

// Options returns the step options
func (step *DescriptorStepBSON) Options() map[string]string {
	return step.options
}

// Store serializes this descriptor step (operation not implemented)
func (step *DescriptorStepBSON) Store() ([]byte, error) {
	dest := &descriptorStepBSONInternal{
		Cmd:          step.cmd,
		Dependencies: step.dependencies,
		Name:         step.name,
		Options:      step.options,
		ID:           step.id,
	}

	data, err := bson.Marshal(dest)
	return data, errors.Because(err, err, "Store: Unable to store descriptor step in a byte slice")
}

// Load deserializes the given byte slice into a descriptor step (operation not implemented)
func (step *DescriptorStepBSON) Load(data []byte) error {
	dest := &descriptorStepBSONInternal{}
	err := bson.Unmarshal(data, dest)
	if err != nil {
		return errors.Because(err, err, "Load: Unable to load slice as a descriptor step")
	}

	step.loadFrom(dest)

	return nil
}

func (step *DescriptorStepBSON) loadFrom(other *descriptorStepBSONInternal) {
	srvs := utilities.NewStringSlice(step.cmd)
	for _, v := range other.Cmd {
		srvs.AppendIf(v)
	}
	step.cmd = srvs.Cast()

	srvs = utilities.NewStringSlice(step.dependencies)
	for _, v := range other.Dependencies {
		srvs.AppendIf(v)
	}
	step.dependencies = srvs.Cast()

	for key, value := range other.Options {
		step.options[key] = value
	}

	step.name = other.Name
	step.id = other.ID
}

type descriptorBSONInternal struct {
	Name     string
	Hash     string
	Image    string
	Services []string
	Steps    []*descriptorStepBSONInternal
}

type descriptorStepBSONInternal struct {
	Cmd          []string          `bson:"command"`
	Dependencies []string          `bson:"dependencies"`
	Name         string            `bson:"name"`
	Options      map[string]string `bson:"options"`
	ID           string            `bson:"id"`
}
