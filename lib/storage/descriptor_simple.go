// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package storage

import (
	"gopkg.in/errgo.v2/fmt/errors"
)

// DescriptorSimple is a concrete descriptor that is used to transfer data
type DescriptorSimple struct {
	IName     string                  // the name of the project
	IHash     string                  // the hash of the descriptor file
	IImage    string                  // the docker image to use for the main build process
	IServices []string                // the docker image for the services used by the build process
	ISteps    []*DescriptorStepSimple // the build steps by topological order (ie. ready to be executed)
}

// Name is the name of the project
func (project *DescriptorSimple) Name() string {
	return project.IName
}

// Hash is the hash of the descriptor file
func (project *DescriptorSimple) Hash() string {
	return project.IHash
}

// Image is the docker image to use for the main build process
func (project *DescriptorSimple) Image() string {
	return project.IImage
}

// Services are the docker images for the services used by the build process
func (project *DescriptorSimple) Services() []string {
	return project.IServices
}

// Steps are the build steps
func (project *DescriptorSimple) Steps() []DescriptorStep {
	steps := make([]DescriptorStep, len(project.ISteps))
	for i, v := range project.ISteps {
		steps[i] = DescriptorStep(v)
	}
	return steps
}

// Store serializes this descriptor (operation not implemented)
func (project *DescriptorSimple) Store() ([]byte, error) {
	return nil, errors.New("Operation not implemented")
}

// Load deserializes the given byte slice into a descriptor (operation not implemented)
func (project *DescriptorSimple) Load(data []byte) error {
	return errors.New("Operation not implemented")
}

//DescriptorStepSimple is a build step implementation that is used to transfer data
type DescriptorStepSimple struct {
	ICmd          []string          // the step commands
	IDependencies []string          // the step dependencies (between other steps)
	IName         string            // the step name
	IOptions      map[string]string // step options
	IID           string            // the step uuid
}

// Cmd are the step commands
func (step *DescriptorStepSimple) Cmd() []string {
	return step.ICmd
}

// Dependencies are the step dependencies
func (step *DescriptorStepSimple) Dependencies() []string {
	return step.IDependencies
}

// Name is the step name
func (step *DescriptorStepSimple) Name() string {
	return step.IName
}

// ID is the step uuid
func (step *DescriptorStepSimple) ID() string {
	return step.IID
}

// Options returns the step options
func (step *DescriptorStepSimple) Options() map[string]string {
	return step.IOptions
}

// Store serializes this descriptor step (operation not implemented)
func (step *DescriptorStepSimple) Store() ([]byte, error) {
	return nil, errors.New("Operation not implemented")
}

// Load deserializes the given byte slice into a descriptor step (operation not implemented)
func (step *DescriptorStepSimple) Load(data []byte) error {
	return errors.New("Operation not implemented")
}
