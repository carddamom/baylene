// Copyright (c) 2019 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package command

import (
	treemap "github.com/emirpasic/gods/maps/treemap"
	juju "github.com/juju/errors"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

//ArgManager is represents the current Argument Manager
var ArgManager = NewArgumentManager()

//Command represents a command line command
type Command interface {
	Configure(cmd *kingpin.CmdClause) error
	PreRun(ctx *kingpin.ParseContext) error
	Run(ctx *kingpin.ParseContext) error
	Validate(ctx *kingpin.CmdClause) error
}

//ArgumentManager represents an argument manager, that is able to parse command line arguments
type ArgumentManager struct {
	app    *kingpin.Application
	cmdmap *treemap.Map
}

//App returns the kingpin object representing the argument parser
func (argman *ArgumentManager) App() *kingpin.Application {
	return argman.app
}

//CmdMap returns a map with all available commands indexed by the command name
func (argman *ArgumentManager) CmdMap() *treemap.Map {
	return argman.cmdmap
}

//Register registers a new command with the manager
func (argman *ArgumentManager) Register(cmd Command, name string, description string) error {
	command := argman.app.Command(name, description)
	err := cmd.Configure(command)
	if err != nil {
		return juju.Annotatef(err, "Error while registering a new command")
	}
	argman.cmdmap.Put(name, cmd)
	return nil
}

//Unregister unregisters an existing command with the manager
func (argman *ArgumentManager) Unregister(cmd Command, name string) error {
	_, found := argman.cmdmap.Get(name)
	if !found {
		return juju.NotFoundf("The command (%s) was not registered", name)
	}
	return nil
}

//GetCommand returns the command with the given name or an error if the command does not exist
func (argman *ArgumentManager) GetCommand(name string) (Command, error) {
	obj, found := argman.cmdmap.Get(name)
	if !found {
		return nil, juju.NotFoundf("The command (%s) was not found", name)
	}
	cmd, ok := obj.(Command)
	if !ok {
		return nil, juju.NotFoundf("The command (%s) was not found", name)
	}
	return cmd, nil
}

//NewArgumentManager creates a new argument manager
func NewArgumentManager() *ArgumentManager {
	result := &ArgumentManager{
		app:    kingpin.New("baylene", "The local continous integration tool"),
		cmdmap: treemap.NewWithStringComparator(),
	}

	result.app.Author("carddamom")
	result.app.Version("0.0.2")

	return result
}
