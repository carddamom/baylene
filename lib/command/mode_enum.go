// generated code - do not edit
// bitbucket.org/rickb777/enumeration v1.2.0

package command

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const modeEnumStrings = "MachineReadableModeHumanReadableMode"

var modeEnumIndex = [...]uint16{0, 19, 36}

// AllModes lists all 2 values in order.
var AllModes = []Mode{MachineReadableMode, HumanReadableMode}

// String returns the string representation of a Mode.
func (i Mode) String() string {
	o := i.Ordinal()
	if o < 0 || o >= len(AllModes) {
		return fmt.Sprintf("Mode(%d)", i)
	}
	return modeEnumStrings[modeEnumIndex[o]:modeEnumIndex[o+1]]
}

// Ordinal returns the ordinal number of a Mode.
func (i Mode) Ordinal() int {
	switch i {
	case MachineReadableMode:
		return 0
	case HumanReadableMode:
		return 1
	}
	return -1
}

// ModeOf returns a Mode based on an ordinal number. This is the inverse of Ordinal.
// If the ordinal is out of range, an invalid Mode is returned.
func ModeOf(i int) Mode {
	if 0 <= i && i < len(AllModes) {
		return AllModes[i]
	}
	// an invalid result
	return MachineReadableMode + HumanReadableMode
}

// IsValid determines whether a Mode is one of the defined constants.
func (i Mode) IsValid() bool {
	switch i {
	case MachineReadableMode, HumanReadableMode:
		return true
	}
	return false
}

// Parse parses a string to find the corresponding Mode, accepting either one of the string
// values or an ordinal number.
func (v *Mode) Parse(s string) error {
	ord, err := strconv.Atoi(s)
	if err == nil && 0 <= ord && ord < len(AllModes) {
		*v = AllModes[ord]
		return nil
	}
	var i0 uint16 = 0
	for j := 1; j < len(modeEnumIndex); j++ {
		i1 := modeEnumIndex[j]
		p := modeEnumStrings[i0:i1]
		if s == p {
			*v = AllModes[j-1]
			return nil
		}
		i0 = i1
	}
	return errors.New(s + ": unrecognised Mode")
}

// AsMode parses a string to find the corresponding Mode, accepting either one of the string
// values or an ordinal number.
func AsMode(s string) (Mode, error) {
	var i = new(Mode)
	err := i.Parse(s)
	return *i, err
}

// MarshalText converts values to a form suitable for transmission via JSON, XML etc.
func (i Mode) MarshalText() (text []byte, err error) {
	return []byte(i.String()), nil
}

// UnmarshalText converts transmitted values to ordinary values.
func (i *Mode) UnmarshalText(text []byte) error {
	return i.Parse(string(text))
}

// ModeMarshalJSONUsingString controls whether generated JSON uses ordinals or strings. By default,
// it is false and ordinals are used. Set it true to cause quoted strings to be used instead,
// these being easier to read but taking more resources.
var ModeMarshalJSONUsingString = false

// MarshalJSON converts values to bytes suitable for transmission via JSON. By default, the
// ordinal integer is emitted, but a quoted string is emitted instead if
// ModeMarshalJSONUsingString is true.
func (i Mode) MarshalJSON() ([]byte, error) {
	if ModeMarshalJSONUsingString {
		s := []byte(i.String())
		b := make([]byte, len(s)+2)
		b[0] = '"'
		copy(b[1:], s)
		b[len(s)+1] = '"'
		return b, nil
	}
	// else use the ordinal
	s := strconv.Itoa(i.Ordinal())
	return []byte(s), nil
}

// UnmarshalJSON converts transmitted JSON values to ordinary values. It allows both
// ordinals and strings to represent the values.
func (i *Mode) UnmarshalJSON(text []byte) error {
	// Ignore null, like in the main JSON package.
	if string(text) == "null" {
		return nil
	}
	s := strings.Trim(string(text), "\"")
	return i.Parse(s)
}
