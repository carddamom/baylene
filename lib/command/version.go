// Copyright (c) 2019 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package command

import (
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

//VersionCommand represents a Version command
type VersionCommand struct {
	verbose bool
	config  string
	dryrun  bool
	command *kingpin.CmdClause
	valid   bool
}

//Configure configures this command using the given kingpin object
func (cmd *VersionCommand) Configure(cmd2 *kingpin.CmdClause) error {
	cmd.command = cmd2

	cmd2.Flag("verbose", "If more verbose output is required").Short('v').BoolVar(&cmd.verbose)
	cmd2.Flag("config", "The configuration file to use").Short('c').StringVar(&cmd.config)
	cmd2.Flag("dryrun", "It makes the command execute everything but without doing its action just printing what it would do").
		Short('n').BoolVar(&cmd.dryrun)

	return nil
}

//PreRun executes before validatation
func (cmd *VersionCommand) PreRun(ctx *kingpin.ParseContext) error {
	return nil
}

//Run represents the version command action
func (cmd *VersionCommand) Run(ctx *kingpin.ParseContext) error {
	cmd.valid = true
	return nil
}

//Validate checks if all of the command arguments are valid
func (cmd *VersionCommand) Validate(ctx *kingpin.CmdClause) error {
	return nil
}

//NewVersionCommand creates a new version command with the right defaults
func NewVersionCommand() *VersionCommand {
	return &VersionCommand{
		verbose: false,
		config:  "",
		dryrun:  false,
		command: nil,
		valid:   false,
	}
}
