// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package command

import (
	"fmt"
	path "path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/afero"
	"github.com/urfave/cli"
	configuration "gitlab.com/carddamom/baylene/lib/configuration"
	descriptor "gitlab.com/carddamom/baylene/lib/descriptor/baylene/v1"
	"gopkg.in/errgo.v2/fmt/errors"
)

// AddCommand represents the add argument
type AddCommand struct {
	logger     *log.Logger
	project    string
	name       string
	fullDir    string
	descriptor string
	config     string
}

// NewAddCommand creates a new add command
func NewAddCommand(logger *log.Logger) *AddCommand {
	return &AddCommand{logger, "", "", "", "", ""}
}

// Before is the action executed before the command is run
func (cmd *AddCommand) Before(context *cli.Context) error {
	if context.String("project") == "" || context.String("name") == "" {
		fmt.Println("Error: Missing required arguments")
		cli.ShowCommandHelp(context, "add")
		return errors.New("Add command does not contain the required arguments")
	}
	return nil
}

// After is the action executed after the command is run
func (cmd *AddCommand) After(context *cli.Context) error {
	return nil
}

// Run is the action executed by the command
func (cmd *AddCommand) Run(context *cli.Context) error {
	cmd.name = context.String("name")
	cmd.project = context.String("project")
	cmd.config = context.String("configuration")

	log.Infof("Configuration file is %s", cmd.config)

	fs := afero.NewOsFs()

	var config *configuration.Configuration

	//Find out the location of the configuration file and open it
	exists, err := afero.Exists(fs, cmd.config)
	if exists != false && err == nil {
		//Parse the cofiguration file
		config, err = configuration.LoadFromJSON(cmd.config)
		if err != nil {
			config = &configuration.Configuration{}
		}
	} else {
		config = &configuration.Configuration{}
	}

	//Find out the project directory
	exists, err = afero.DirExists(fs, cmd.project)
	if exists == false || err != nil {
		if err != nil {
			return errors.Becausef(err, err, "Add: Unable to find project directory %s.", cmd.project)
		}
		return errors.Newf("Add: Unable to find project directory %s.", cmd.project)
	}
	cmd.logger.Infof("Project directory (%s) found!", cmd.project)

	//Check if it contains a file baylene-descriptor.json
	cmd.fullDir, err = path.Abs(cmd.project)
	if err != nil {
		return errors.Becausef(err, err, "Add: Unable to resolve project directory %s.", cmd.project)
	}
	cmd.logger.Infof("Absolute project directory is %s", cmd.fullDir)

	cmd.descriptor = path.Join(cmd.fullDir, "baylene-descriptor.json")
	exists, err = afero.Exists(fs, cmd.descriptor)
	if exists == false || err != nil {
		if err != nil {
			return errors.Becausef(err, err, "Add: Unable to find project descriptor %s.", cmd.descriptor)
		}
		return errors.Newf("Add: Unable to find project descriptor %s.", cmd.descriptor)
	}
	cmd.logger.Infof("Baylene descriptor (%s) found!", cmd.descriptor)

	//Validate the file, using the json schema
	err = descriptor.ValidateDescriptor(cmd.descriptor)
	if err != nil {
		return errors.Wrap(err)
	}
	cmd.logger.Infof("Baylene descriptor (%s) is valid", cmd.descriptor)

	//Parse the baylene-descriptor file
	desc, err := descriptor.NewBayleneDescriptorFromJSONFile(cmd.descriptor)
	if err != nil {
		return errors.Wrap(err)
	}
	cmd.logger.Infof("Baylene descriptor (%s) successfully loaded", cmd.descriptor)

	cmd.logger.Debugf("Descriptor version is %s", desc.Version())
	cmd.logger.Debugf("Descriptor hash is %s", *desc.Hash())

	//Get the task dependency graph of the descriptor file

	//Write the descriptor file into the configuration

	//Save the configuration file changes
	err = config.SaveToJSON(cmd.config)
	if err != nil {
		return errors.Wrap(err)
	}

	cmd.logger.Infof("Baylene descriptor (%s) was added with name %s", cmd.descriptor, cmd.name)
	return nil
}
