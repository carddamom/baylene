// Copyright (c) 2019 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package command

import (
	juju "github.com/juju/errors"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

//ConvertCommand represents a convert command
type ConvertCommand struct {
	iformat string
	oformat string
	input   string
	output  string
	mode    Mode
	smode   string
	verbose bool
	config  string
	dryrun  bool
	command *kingpin.CmdClause
	valid   bool
}

//Configure configures this command using the given kingpin object
func (cmd *ConvertCommand) Configure(cmd2 *kingpin.CmdClause) error {
	cmd.command = cmd2

	cmd2.Flag("iformat", "The format of the input build file").StringVar(&cmd.iformat)
	cmd2.Flag("oformat", "The format of the output build file").StringVar(&cmd.oformat)
	cmd2.Flag("input", "The input build file").Short('i').StringVar(&cmd.input)
	cmd2.Flag("output", "The output build file").Short('o').StringVar(&cmd.output)
	cmd2.Flag("mode", "The mode of output (machine or human readable)").
		Short('m').EnumVar(&cmd.smode, MachineReadableMode.String(), HumanReadableMode.String())

	cmd2.Flag("verbose", "If more verbose output is required").Short('v').BoolVar(&cmd.verbose)
	cmd2.Flag("config", "The configuration file to use").Short('c').StringVar(&cmd.config)
	cmd2.Flag("dryrun", "It makes the command execute everything but without doing its action just printing what it would do").
		Short('n').BoolVar(&cmd.dryrun)

	return nil
}

//PreRun executes before validatation
func (cmd *ConvertCommand) PreRun(ctx *kingpin.ParseContext) error {
	return nil
}

//Run represents the cnvert command action
func (cmd *ConvertCommand) Run(ctx *kingpin.ParseContext) error {
	cmd.valid = true
	return nil
}

//Validate checks if all of the command arguments are valid
func (cmd *ConvertCommand) Validate(ctx *kingpin.CmdClause) error {
	if cmd.smode != "" {
		mode, err := AsMode(cmd.smode)
		if err != nil {
			return juju.Annotatef(err, "The given mode (%s) is not valid", cmd.smode)
		}
		cmd.mode = mode
	}
	if cmd.iformat != "" && cmd.oformat != "" && cmd.iformat == cmd.oformat {
		return juju.New("The input and output formats are the same")
	}
	return nil
}

//NewConvertCommand creates a new convert command with the right defaults
func NewConvertCommand() *ConvertCommand {
	return &ConvertCommand{
		iformat: "",
		oformat: "",
		input:   "",
		output:  "",
		command: nil,
		verbose: false,
		config:  "",
		dryrun:  false,
		mode:    HumanReadableMode,
		valid:   false,
	}
}
