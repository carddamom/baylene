// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package configuration

import (
	"github.com/pquerna/ffjson/ffjson"

	"github.com/spf13/afero"
	errors "gopkg.in/errgo.v2/fmt/errors"
)

// Configuration is the baylene configuration json schema
type Configuration struct {
}

//LoadFromJSON loads a new descriptor from the given file
func LoadFromJSON(filename string) (*Configuration, error) {
	configuration := &Configuration{}

	data, err := afero.ReadFile(afero.NewOsFs(), filename)
	if err != nil {
		return nil, errors.Becausef(err, err, "Load: Unable to read configuration file %s", filename)
	}

	err = ffjson.Unmarshal(data, configuration)
	if err != nil {
		return nil, errors.Becausef(err, err, "Load: Unable to parse configuration file %s", filename)
	}
	return configuration, nil
}

// SaveToJSON writes the current configuration to the given filename
func (config *Configuration) SaveToJSON(filename string) error {
	data, err := ffjson.Marshal(config)
	if err != nil {
		return errors.Becausef(err, err, "Load: Unable to save configuration to file %s", filename)
	}

	err = afero.WriteFile(afero.NewOsFs(), filename, data, 0644)
	if err != nil {
		return errors.Becausef(err, err, "Load: Unable to save configuration to file %s", filename)
	}

	return nil
}
