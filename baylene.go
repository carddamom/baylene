// Copyright (c) 2018 carddamom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package main

import (
	"os"

	log "github.com/go-ozzo/ozzo-log"
	command "gitlab.com/carddamom/baylene/lib/command"
)

func main() {
	logger := log.NewLogger()
	t1 := log.NewConsoleTarget()
	logger.Targets = append(logger.Targets, t1)

	logger.Open()
	defer logger.Close()

	argMan := command.NewArgumentManager()
	argMan.Register(command.NewVersionCommand(), "version", "Shows versioning information")
	argMan.Register(command.NewConvertCommand(), "convert", "Converts between supported build files")
	argMan.Register(command.NewRunCommand(), "run", "Runs the build")
	argMan.Register(command.NewValidateCommand(), "validate", "Validates the given build file")

	_, err := argMan.App().Parse(os.Args[1:])
	if err != nil {
		logger.Error(err.Error())
	}
}
