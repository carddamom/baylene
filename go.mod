module gitlab.com/carddamom/baylene

require (
	github.com/AndreasBriese/bbloom v0.0.0-20180913140656-343706a395b7 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/daaku/go.zipexe v0.0.0-20150329023125-a5fe2436ffcb // indirect
	github.com/dgraph-io/badger v1.5.4
	github.com/dgryski/go-farm v0.0.0-20180109070241-2de33835d102 // indirect
	github.com/emirpasic/gods v1.12.0
	github.com/go-ozzo/ozzo-log v0.0.0-20160703175702-610cdd147d9a
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/homedepot/mgo v0.0.0-20181001212412-e1a82b9f8e2a
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/juju/errors v0.0.0-20190207033735-e65537c515d7
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20190429233213-dfc56b8c09fc // indirect
	github.com/jzelinskie/whirlpool v0.0.0-20170603002051-c19460b8caa6
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1 // indirect
	github.com/mailru/easyjson v0.0.0-20180823135443-60711f1a8329
	github.com/nkovacs/go.rice v0.0.0-20170720194307-4a0384028401
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pquerna/ffjson v0.0.0-20180717144149-af8b230fcd20
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.1.1
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20180222194500-ef6db91d284a
	github.com/spf13/afero v1.1.2
	github.com/tinylib/msgp v1.1.0 // indirect
	github.com/urfave/cli v1.20.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v0.0.0-20181016150526-f3a9dae5b194
	golang.org/x/net v0.0.0-20181106065722-10aee1819953 // indirect
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f // indirect
	golang.org/x/sys v0.0.0-20181026203630-95b1ffbd15a5 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/errgo.v2 v2.1.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
	zombiezen.com/go/capnproto2 v2.17.0+incompatible
)
